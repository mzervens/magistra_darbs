import Overlay from 'ol/Overlay';
import Map from 'ol/Map';

class PopupManager {
    private container = document.getElementById('popup');
    private content = document.getElementById('popup-content');
    private closer = document.getElementById('popup-closer');
    private map: Map;
    private overlay: Overlay;
    
    public constructor(map: Map) {
        this.map = map;
        this.init();
    }

    public show(content, position) {
        this.content.innerHTML = content;
        this.overlay.setPosition(position);
    }

    private init() {
        this.overlay = new Overlay({
            element: this.container,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });

        this.closer.onclick = this.close.bind(this);
        this.map.addOverlay(this.overlay);
    }

    public close() {
        this.overlay.setPosition(undefined);
        this.closer.blur();
        return false;
    }
}

export {PopupManager}