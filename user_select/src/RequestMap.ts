import 'ol/ol.css';
import VectorSource from 'ol/source/vector';
import BingMaps from 'ol/source/BingMaps';
import VectorLayer from 'ol/layer/vector';
import TileLayer from 'ol/layer/tile';
import Draw, {createBox} from 'ol/interaction/Draw';
import Map from 'ol/Map';
import View from 'ol/View';
import TileWMS from 'ol/source/TileWMS';
import {transform} from 'ol/proj';
import { LocalStorage } from './LocalStorage';
import { RegionRequestManager } from './RegionRequestManager';
import { LayerStyleManager } from './LayerStyleManager';



class RequestMap {
	private drawButton = document.getElementById("drawRegionButton");
	private createRegionButton = document.getElementById("createRegionButton");
	private deleteRegionButton = document.getElementById("deleteRegionButton");
	private isDrawEnabled = false;
	private bingMapKey = "Ajfj_tEzYU5bFMVTbaWKdvAXdNwq7MAgjAdRQUtHvoSCo1OH7svzUgE5qN3VpWbx";
	private draw: Draw;
	private drawSource: VectorSource;
	private storage: LocalStorage;
	private requestManager: RegionRequestManager;
	private layerStyleManager: LayerStyleManager;
	private drawnFeatureWkt: string;
	public map: Map;
	public regionRequestSource: VectorSource;


	public constructor(storage: LocalStorage) {
		this.storage = storage;
		this.layerStyleManager = new LayerStyleManager();
		this.initMap();
		this.requestManager = new RegionRequestManager(this);
		this.requestManager.loadRegionRequests();
	}


	private initMap() {
		this.drawSource = new VectorSource({wrapX: false});
		this.regionRequestSource = new VectorSource({wrapX: false});
		const drawLayer = new VectorLayer({
			source: this.drawSource
		});

		const regionRequestStyle = this.layerStyleManager.getStyle();
		const regionRequestLayer = new VectorLayer({
			source: this.regionRequestSource,
			style: (feature) => {
				const processedAt = feature.get('processed_at');
				const fill = regionRequestStyle.getFill();
				if (!processedAt) {
					fill.setColor('rgba(125, 125, 10, 0.6)');
					
				} else {
					fill.setColor('rgba(10, 200, 10, 0.6)');
				}
				regionRequestStyle.getText().setText(feature.get('id').toString());
				return regionRequestStyle;
			}
		});
		
		
		this.draw = new Draw({
			source: this.drawSource,
			type: "Circle",
			geometryFunction: createBox()
		});
		
		this.draw.on('drawend', (e) => {
			this.disableDrawing();
			this.getUserSelection(e);
		});
		


		this.map = new Map({
			target: 'map',
			layers: [
				new TileLayer({
					source: new BingMaps({
						key: this.bingMapKey,
						imagerySet: "Aerial"
					})
				}),
				new TileLayer({
					extent: [2005707.622203, 7362414.564428, 3365675.229453, 8230739.205748],
					source: new TileWMS({
					  url: 'http://localhost:8080/geoserver/cite/wms?',
					  params: {'LAYERS': 'cite:test'},
					  serverType: 'geoserver',
					  // Countries have transparency, so do not fade tiles:
					  transition: 0
					})
				  }),
				drawLayer,
				regionRequestLayer
			],
			view: new View({
			center: [2801188.7302762214, 7741177.017511494],
			zoom: 8
			})
		});
		
		this.initMapPosition();

		this.drawButton.addEventListener("click", this.toggleDrawing.bind(this));
		this.createRegionButton.addEventListener("click", this.onDrawButtonClick.bind(this));
		this.deleteRegionButton.addEventListener("click", this.onDeleteButtonClick.bind(this));
		this.map.on('moveend', (e) => {
			const center = this.map.getView().getCenter();
			const zoom = this.map.getView().getZoom();
			this.storage.set("zoom", zoom);
			this.storage.set("center", JSON.stringify(center));
			console.log(center, zoom);
		})
	}

	private onDrawButtonClick() {
		this.requestManager.createRegionRequest(this.drawnFeatureWkt).then((id) => {
			if (id) {
				this.drawnFeatureWkt = null;
				this.drawSource.clear();
			}
		})
	}

	private onDeleteButtonClick() {
		this.requestManager.deleteRegion();
	}

	private initMapPosition() {
		const zoom = this.storage.get("zoom");
		const center = this.storage.get("center");
		if (zoom !== null) {
			this.map.getView().setZoom(parseFloat(zoom));
		}

		if (center !== null) {
			this.map.getView().setCenter(JSON.parse(center));
		}

	}
	
	private toggleDrawing(e) {
		if (this.isDrawEnabled) {
			this.disableDrawing();
		} else {
			this.enableDrawing(); 
		}	
	}
	
	private enableDrawing() {
		this.drawSource.clear();
		this.map.addInteraction(this.draw);
		this.isDrawEnabled = true;
	}
	
	private disableDrawing() {
		this.map.removeInteraction(this.draw);
		this.isDrawEnabled = false;
	}
	
	private roundCoord(coord) {
		var places = 6;
		var rounded = [
						coord[0].toFixed(places), 
						coord[1].toFixed(places)
					];
		return rounded;
	}
	
	private getUserSelection(e) {
		var feature = e.feature;
		var geometry = feature.getGeometry();
		console.log(geometry);
		var extent = geometry.flatCoordinates;
		var wgs84Extent = [];
		var wktString = "POLYGON((";
		for(var i = 0; i < extent.length; i = i + 2) {
			var coord = [extent[i], extent[i + 1]];
			var wgs84Coord = transform(coord, 'EPSG:3857', 'EPSG:4326');
			var seperator = i ? ", " : "";
			
			wgs84Coord = this.roundCoord(wgs84Coord);
			wktString = wktString + seperator + wgs84Coord[0] + " " + wgs84Coord[1];
			wgs84Extent.push(wgs84Coord);
		}
	
		wktString = wktString + "))";
		this.drawnFeatureWkt = wktString;
		console.log(wktString);
	}
}

export {RequestMap}
