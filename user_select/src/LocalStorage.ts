
class LocalStorage {
    private storage = window.localStorage;
    public constructor() {

    }

    public get(key: string) {
        return this.storage.getItem(key);
    }

    public set(key: string, value: string) {
        this.storage.setItem(key, value);
    }
}

export {LocalStorage}