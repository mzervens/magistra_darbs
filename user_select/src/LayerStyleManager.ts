import {Fill, Stroke, Style, Text} from 'ol/style';

class LayerStyleManager {
    public constructor() {

    }

    public getStyle(): Style {
        const style = new Style({
            fill: new Fill({
              color: 'rgba(255, 255, 255, 0.6)'
            }),
            stroke: new Stroke({
              color: '#319FD3',
              width: 1
            }),
            text: new Text({
              font: '12px Calibri,sans-serif',
              fill: new Fill({
                color: '#000'
              }),
              stroke: new Stroke({
                color: '#fff',
                width: 3
              })
            })
        });

        return style;
    }
}

export {LayerStyleManager}