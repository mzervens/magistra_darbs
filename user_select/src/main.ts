import {RequestMap} from './RequestMap';
import { LocalStorage } from './LocalStorage';

const storage = new LocalStorage();
const requestMap = new RequestMap(storage);