import {SERVER} from './config';
import WKT from 'ol/format/WKT';
import Select from 'ol/interaction/Select';
import {getCenter} from 'ol/extent';
import axios from 'axios';
import { RequestMap } from './RequestMap';
import { PopupManager } from './PopupManager';

class RegionRequestManager {
    private requestMap: RequestMap;
    private popupManager: PopupManager;
    private selectInteraction: Select;
    private toggleProcessingId = "toggleProcessing";
    private selectedRegionId: number;

    public constructor(requestMap: RequestMap) {
        this.requestMap = requestMap;
        this.selectInteraction = new Select();
        this.popupManager = new PopupManager(requestMap.map);
        this.selectInteraction.on('select', this.onRegionSelect.bind(this));
        document.addEventListener('click', this.onProcessingToggle.bind(this));
        requestMap.map.addInteraction(this.selectInteraction);
    }

    public createRegionRequest(regionBboxWkt: string) {
        return new Promise((resolve) => {
            axios.post(SERVER + "/create_request", {
                bbox: regionBboxWkt
            }).then((response) => {
                if (response.status === 200) {
                    this.loadRegionRequests();
                    resolve(response.data);
                    return;
                }
                resolve(null);
            }).catch((err) => {
                console.log(err);
                resolve(null);
            })
        });
    }

    public deleteRegion() {
        if (!this.selectedRegionId) {
            return;
        }

        this.postServer("/delete_request", {id: this.selectedRegionId}).then((id) => {
            if (!id) {
                return;
            }
            this.loadRegionRequests();
            this.popupManager.close();
        });
    }

	public loadRegionRequests() {
		axios.get(SERVER + "/get_regions")
		.then((response) => {
		  // handle success
		  this.displayRegionRequests(response.data);
		  console.log(response);
		})
		.catch((error) => {
		  // handle error
		  console.log(error);
		});
	
    }

    public isRegionProcessing(regionId) {
        return this.postServer("/is_region_processing", {id: regionId});
    }

    private async showRegionInformation(feature) {
        const featureId = feature.get('id').toString();
        const featureCenter = getCenter(feature.getGeometry().getExtent());

        this.isRegionProcessing(featureId).then((isProcessing: boolean) => {
            const content = this.getRegionRequestPopupContent(featureId, isProcessing);
            this.popupManager.show(content, featureCenter);
            console.log(featureId, featureCenter, isProcessing);
        })

    }

    private getRegionRequestPopupContent(featureId: number, isProcessing: boolean) {
        let content = "<div><b>Request id:</b> "+featureId+"<br/>";
        let button = "<button data-id='"+featureId+"' id='"+this.toggleProcessingId+"'>";
        if (isProcessing) {
            button += "Stop processing";
        } else {
            button += "Start processing";
        }
        button += "</button>";
        content = content + button + "</div>"
        return content
    }

    private startProcessing(id: string) {
        return this.postServer("/start_processing", {id});
    }

    private stopProcessing(id: string) {
        return this.postServer("/stop_processing", {id});
    }

    private postServer(url: string, data) {
        return new Promise((resolve) => {
            axios.post(SERVER + url, data).then((response) => {
                if (response.status === 200) {
                    resolve(response.data);
                    return;
                }
                resolve(null);
            }).catch((err) => {
                console.log(err);
                resolve(null);
            })
        });
    }

    private async onProcessingToggle(e) {
        if(!e.target || e.target.id !== this.toggleProcessingId){
           return;
        }

        const id = e.target.getAttribute("data-id"); 
        const isProcessing = await this.isRegionProcessing(id);
        if (isProcessing) {
            this.stopProcessing(id).then((id) => {
                e.target.innerHTML = "Start processing";
            });
        } else {
            this.startProcessing(id).then((id) => {
                if (id) {
                    e.target.innerHTML = "Stop processing";
                }
            });
        }
    }

    private onRegionSelect(e) {
        const features = e.target.getFeatures().getArray();
        if (!features.length) {
            this.popupManager.close();
            console.log(this.selectInteraction);
            return;
        }
        const regionId = features[0].get('id');
        if (regionId) {
            this.selectedRegionId = regionId;
            this.showRegionInformation(features[0]);
        } else {
            this.selectedRegionId = null;
        } 
        
    }
    
    private displayRegionRequests(regionRequests: any[]) {
		const format = new WKT();
		const dataProjection = "EPSG:4326";
		const featureProjection = "EPSG:3857";
		const features = [];

		for (const regionRequest of regionRequests) {
			var feature = format.readFeature(regionRequest.wkt, {
				dataProjection,
				featureProjection
			});
			feature.set("id", regionRequest.id);
			feature.set("processed_at", regionRequest.processed_at);
			features.push(feature);
		}

		this.requestMap.regionRequestSource.clear();
		this.requestMap.regionRequestSource.addFeatures(features);
	}
}

export {RegionRequestManager};