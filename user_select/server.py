from flask import Flask, request, jsonify
from flask_cors import CORS
import atexit
import threading
from image_processing.database.connect import conn, cur
from image_processing.process_region.main import process_region

class ThreadManager:
    def __init__(self):
        self.stopThreads = False
        self.stopSpecificThreads = {}
        self.runningThreads = {}

    def is_thread_running(self, id):
        if not id in self.runningThreads:
            return False
        
        thread = self.runningThreads[id]
        return thread.isAlive()

    def add(self, id, region):
        if self.is_thread_running(id):
            return

        thread = threading.Thread(target = process_region, args = (id, region, self))
        self.stopSpecificThreads[id] = False
        self.runningThreads[id] = thread
        thread.start()

    def remove(self, id):
        if not id in self.runningThreads:
            return

        self.stop_thread(id)
        del self.stopSpecificThreads[id]
        del self.runningThreads[id]

    def should_stop(self, id):
        return self.stopThreads or self.stopSpecificThreads[id]

    def stop(self):
        self.stopThreads = True

    def start(self):
        self.stopThreads = False

    def stop_threads(self):
        self.stop()
        for id, thread in self.runningThreads.items():
            thread.join()
        print("threads closed")

    def stop_thread(self, id):
        if not id in self.runningThreads:
            return

        self.stopSpecificThreads[id] = True
        thread = self.runningThreads[id]
        thread.join()

threadManager = ThreadManager()
app = Flask(__name__)
CORS(app)

atexit.register(threadManager.stop_threads)

def get_region(id):
    sql = "SELECT *, ST_AsText(geom) as wkt FROM region_request WHERE id = %s"
    cur.execute(sql, [id])
    return cur.fetchone()

@app.route('/get_regions', methods=['GET'])
def get_regions():
    sql = "SELECT *, ST_AsText(geom) as wkt FROM region_request"
    cur.execute(sql)
    return jsonify(cur.fetchall())

@app.route('/check_region', methods=['POST'])
def check_region():
    content = request.get_json(silent=False)
    bbox = content['bbox']
    sql = "SELECT *, ST_AsText(geom) as wkt FROM region_request WHERE ST_Intersects(geom, ST_GeomFromText(%s, 4326))"
    cur.execute(sql, [bbox])
    return jsonify(cur.fetchall())

@app.route('/create_request', methods=['POST'])
def create_request():
    content = request.get_json(silent=False)
    bbox = content['bbox']
    sql = "INSERT INTO region_request (geom) VALUES(ST_GeomFromText(%s, 4326)) RETURNING id"
    cur.execute(sql, [bbox])
    id = cur.fetchone()["id"]
    conn.commit()
    return jsonify(id)

@app.route('/is_region_processing', methods=['POST'])
def is_region_processing():
    content = request.get_json(silent=False)
    id = int(content['id'])
    return jsonify(threadManager.is_thread_running(id))

@app.route('/start_processing', methods=['POST'])
def start_processing():
    content = request.get_json(silent=False)
    id = int(content['id'])
    regionInfo = get_region(id)
    threadManager.add(id, regionInfo)
    return jsonify(id)


@app.route('/stop_processing', methods=['POST'])
def stop_processing():
    content = request.get_json(silent=False)
    id = int(content['id'])
    threadManager.remove(id)
    return jsonify(id)

@app.route('/delete_request', methods=['POST'])
def delete_request():
    content = request.get_json(silent=False)
    id = int(content['id'])
    threadManager.remove(id)
    tileSql = "DELETE FROM region_tile WHERE region_request_id = %s"
    regionSql = "DELETE FROM region_request WHERE id = %s"
    cur.execute(tileSql, [id])
    cur.execute(regionSql, [id])
    conn.commit()
    return jsonify(id)