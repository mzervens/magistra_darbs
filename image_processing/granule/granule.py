import os
from xml.dom import minidom
from skimage import io
import imageio
import numpy as np
from enum import Enum
from image_processing.world_file.reader import Reader
import cv2

class GranuleSizePX(Enum):
    TEN = 10980
    TWENTY = 5490
    SIXTY = 1830

class GranuleResolution(Enum):
    TEN = 10
    TWENTY = 20
    SIXTY = 60

class Granule:
    def __init__(self, config, granuleFolderName):
        self.config = config
        self.imageExtension = ".jp2"
        self.dataFile = "MTD_MSIL1C.xml"
        self.inspireFile = "INSPIRE.xml"
        self.manifestFile = "manifest.safe"
        self.clearFilename = "clear"
        self.clearMaskFilename = "clear_mask"
        self.noDataFilename = "no_data_calculation";
        self.finishedFileName = "finished";
        self.combineMaskName = "combine";
        self.id = granuleFolderName
        self.projection = 0
        self.minPoint = 0
        self.maxPoint = 0
        self.folderName = granuleFolderName
        self.BBOX = []
        self.QUANTIFICATION_VALUE = 10000 
        self.NO_DATA = 0
        self.bandPaths = {}

        self.initialize_folder_name()
        self.initialize_projection_bbox()
        self.initialize_image_paths()
        self.initialize_bbox()

    def get_request_region_bbox(self, id):
        reader = Reader(self.config)
        wordFileInfo = reader.read(self, id)
        image = self.get_request_region_image(id)
        xMax = wordFileInfo["resolution"] * image.shape[1] + wordFileInfo["xMin"]
        yMin = wordFileInfo["yMax"] - (wordFileInfo["resolution"] * image.shape[0])
        return [wordFileInfo["xMin"], yMin, xMax, wordFileInfo["yMax"]]

    def initialize_folder_name(self):
        path = self.config.GRANULE_DIRECTORY + "/" + self.folderName
        for entry in os.listdir(path):
            if os.path.isdir(path + "/" + entry):
                filename = os.fsdecode(entry)
                self.folderName = self.folderName + "/" + filename
                return

    def store_request_region_image(self, id, image):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" + str(id) + '.png', image)       

    def has_request_region_image(self, id):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return os.path.exists(dataPath + "/" + str(id) + '.png')

    def get_request_region_image(self, id):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + str(id) + '.png')

    def has_clear_image(self):
         dataPath = os.path.dirname(self.get_band_path("B01"))
         return os.path.exists(dataPath + "/" + self.clearFilename + '.png') and os.path.exists(dataPath + "/" + self.clearMaskFilename + '.png')

    def get_clear_image(self):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + self.clearFilename + '.png')

    def get_clear_mask(self):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + self.clearMaskFilename + '.png')

    def store_no_data_mask(self, id, image):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" +  str(id) + "_" + self.noDataFilename + '.png', image)

    def get_no_data_mask(self, id):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" +  str(id) + "_" + self.noDataFilename + '.png')

    def store_clear_image(self, image, mask):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" + self.clearFilename + '.png', image)
        imageio.imwrite(dataPath + "/" + self.clearMaskFilename + '.png', mask)
    
    def get_finished_request_image(self, requestId):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + str(requestId) + "_" + self.finishedFileName + '.png')      

    def store_finished_request_image(self, requestId, image):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" + str(requestId) + "_" + self.finishedFileName + '.png', image)
        
    def store_request_region_combining_mask(self, id, image):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" + str(id) + "_" + self.clearMaskFilename + "_" + self.combineMaskName + '.png', image)   

    def store_request_region_mask(self, id, mask):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        imageio.imwrite(dataPath + "/" + str(id) + "_" + self.clearMaskFilename + '.png', mask)  

    def get_request_region_mask(self, id):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + str(id) + "_" + self.clearMaskFilename + '.png')

    def get_request_region_combining_mask(self, id):
        dataPath = os.path.dirname(self.get_band_path("B01"))
        return io.imread(dataPath + "/" + str(id) + "_" + self.clearMaskFilename + "_" + self.combineMaskName + '.png')       

    def get_request_region_mask_scaled(self, id, shape):
        mask = self.get_request_region_mask(id)
        scaledMask = cv2.resize(mask, (shape[1], shape[0]), interpolation=cv2.INTER_CUBIC)
        return scaledMask

    def get_pixel_radiance(self, DN): #https://sentinel.esa.int/documents/247904/685211/Sentinel-2-Products-Specification-Document
        #The conversion formulae to apply to image Digital Numbers (DN) to obtain physical values is Reflectance (float) = DC / (QUANTIFICATION_VALUE) 
        return DN / self.QUANTIFICATION_VALUE;

    def find_coverage_percentage(self, id, hasClearMask = False, clearMask = None):
        if not hasClearMask:
            clearMask = self.get_request_region_mask(id)
        noDataMask = self.get_no_data_mask(id)

        noDataValues = np.count_nonzero(noDataMask==0)
        nonZeroValues = np.count_nonzero(clearMask)
        #print(clearMask.shape)
        size = (clearMask.shape[0] * clearMask.shape[1]) - noDataValues
        if size <= 0:
            return 0
        
        return nonZeroValues / size

    def get_bbox_value(self, bboxNode, boundName):
        boundNode = bboxNode.getElementsByTagName(boundName)[0]
        bound = boundNode.getElementsByTagName('gco:Decimal')[0].childNodes[0].nodeValue
        return float(bound)

    def set_bbox_value(self, bbox):
        self.BBOX = bbox

    def initialize_bbox(self):
        inspireFilePath = self.config.GRANULE_DIRECTORY + "/" + self.folderName + "/" + self.inspireFile
        xmldoc = minidom.parse(inspireFilePath)
        identificationNode = xmldoc.getElementsByTagName('gmd:identificationInfo')[0]
        identificationNode = identificationNode.getElementsByTagName('gmd:MD_DataIdentification')[0]
        extentNode = identificationNode.getElementsByTagName('gmd:extent')[0]
        extentNode = extentNode.getElementsByTagName('gmd:EX_Extent')[0]
        bboxNode = extentNode.getElementsByTagName('gmd:geographicElement')[0]
        bboxNode = bboxNode.getElementsByTagName('gmd:EX_GeographicBoundingBox')[0]

        west = self.get_bbox_value(bboxNode, 'gmd:westBoundLongitude')
        east = self.get_bbox_value(bboxNode, 'gmd:eastBoundLongitude') 
        south = self.get_bbox_value(bboxNode, 'gmd:southBoundLatitude')
        north = self.get_bbox_value(bboxNode, 'gmd:northBoundLatitude')  
        #self.BBOX = [west, south, east, north] not working precisely here

    def get_element_with_attr_val(self, elements, attribute, value):
        for element in elements:
            if element.getAttribute(attribute) == value:
                return element
        return None

    def get_metadata_path_fragment(self):
        manifestPath = self.config.GRANULE_DIRECTORY + "/" + self.folderName + "/" + self.manifestFile
        xmldoc = minidom.parse(manifestPath)
        dataObjSection = xmldoc.getElementsByTagName('dataObjectSection')[0]
        dataObjectElements = dataObjSection.getElementsByTagName('dataObject')

        metadataElement = self.get_element_with_attr_val(dataObjectElements, "ID", "S2_Level-1C_Tile1_Metadata")
        fileLocation = metadataElement.getElementsByTagName('fileLocation')[0]
        path = fileLocation.getAttribute('href')
        return path

    def initialize_projection_bbox(self, resolution = 10, sizePx = 10980):
        pathFragment = self.get_metadata_path_fragment()
        metadataPath = self.config.GRANULE_DIRECTORY + "/" + self.folderName + pathFragment
        xmldoc = minidom.parse(metadataPath)
        geometricInfo = xmldoc.getElementsByTagName('n1:Geometric_Info')[0]
        tileGeocoding = geometricInfo.getElementsByTagName('Tile_Geocoding')[0]
        projectionCode =  tileGeocoding.getElementsByTagName('HORIZONTAL_CS_CODE')[0]
        projCodeValue = projectionCode.childNodes[0].nodeValue.replace("EPSG:", "")
        self.projection = projCodeValue

        geoposition = tileGeocoding.getElementsByTagName('Geoposition')[0]
        ULX = float(geoposition.getElementsByTagName('ULX')[0].childNodes[0].nodeValue)
        ULY = float(geoposition.getElementsByTagName('ULY')[0].childNodes[0].nodeValue)

        maxX = ULX + (resolution * sizePx)
        minY = ULY - (resolution * sizePx)
        self.BBOX = [ULX, minY, maxX, ULY] 


        #print(" init proj ", self.projection, self.BBOX)

        

    def initialize_image_paths(self):
        dataFilePath = self.config.GRANULE_DIRECTORY + "/" + self.folderName + "/" + self.dataFile
        xmldoc = minidom.parse(dataFilePath)
        productOrganization = xmldoc.getElementsByTagName('Product_Organisation')[0]
        granuleList = productOrganization.getElementsByTagName('Granule_List')[0]
        granule = granuleList.getElementsByTagName('Granule')[0]
        bands = granule.getElementsByTagName('IMAGE_FILE')

        for index, band in enumerate(bands):
            bandId = "B"
            if index < 8:
                bandId = bandId + "0" + str(index + 1)
            elif index == 8:
                bandId = bandId + "8A"
            elif index == 9:
                bandId = bandId + "0" + str(index)
            else:
                bandId = bandId + str(index)
            self.bandPaths[bandId] = band.childNodes[0].nodeValue
    def get_band_data(self, bandId):
        path = self.get_band_path(bandId)
        return io.imread(path)
        
    def get_band_path(self, bandId):
        return self.config.GRANULE_DIRECTORY + "/" + self.folderName + "/" + self.bandPaths[bandId] + self.imageExtension
