import image_processing.app_config.config as config
from image_processing.granule.granule import Granule
from image_processing.image_region_extract.extractor import Extractor, ImageType
from image_processing.image_region_extract.combiner import Combiner
from image_processing.granule_extract.search.search_region import SearchRegion, SearchRegionEntry, SearchRegionEntryDataType, IngestionTimeType
from image_processing.database.connect import conn, cur
from image_processing.granule_coverage.request_coverage import RequestCoverage
from image_processing.granule_coverage.request_tile_coverage import RequestTileCoverage
from image_processing.granule_extract.download.download_granule import DownloadGranule
from image_processing.detector.decision_tree.decision_tree_detector import DecisionTreeDetector
from image_processing.world_file.writer import Writer
import imageio
import dateutil.parser
from datetime import datetime, timedelta
import numpy as np


array = np.arange(9).reshape(3,3)
array2 = np.arange(4).reshape(2, 2)

common = np.zeros((3, 3), dtype="uint8")
#print(array[2:3,1:3])
#print(array2[0:1, 0:2])
common[2:3,1:3] = array2[0:1, 0:2]
print(common)


def find_latest_granule_coverage(coverage, regionWkt, days, manager):
    aoiFilter = '"Intersects('+regionWkt+')"'
    searchRegion = SearchRegion(config)
    daysTo = 0
    if days > 4:
        daysTo = days - 4

    granuleEntries = searchRegion.find_granules_in_aoi(aoiFilter, days, daysTo)
    coverage.add_granules_covering_region(granuleEntries, manager)

    if coverage.is_coverage_complete():
        return coverage.get_granules_covering_region()
    else:
        notCoveredRegion = coverage.get_granule_coverage_difference()
        return find_latest_granule_coverage(coverage, notCoveredRegion, days + 4, manager)

def find_older_granule_coverage(coverage, regionWkt, timestampTo, manager):
    aoiFilter = '"Intersects('+regionWkt+')"'
    searchRegion = SearchRegion(config)
    timestampFrom = get_days_before(timestampTo, 4)

    granuleEntries = searchRegion.find_granules_in_aoi(aoiFilter, timestampFrom, timestampTo, IngestionTimeType.TIMESTAMP)
    coverage.add_granules_covering_region(granuleEntries, manager)

    if coverage.is_coverage_complete():
        return coverage.get_granules_covering_region()
    else:
        notCoveredRegion = coverage.get_granule_coverage_difference()
        return find_older_granule_coverage(coverage, notCoveredRegion, timestampFrom, manager)

def download_granule_coverage(id, granuleCoverages, manager):
    downloader = DownloadGranule(config)
    for granuleCoverage in granuleCoverages:
        downloader.download(granuleCoverage.id)
        
        if manager.should_stop(id):
            return

def get_bbox(bboxWkt):
    coords = bboxWkt.replace("POLYGON((", "")
    coordList = coords.split(",")
    minPoint = coordList[0]
    maxPoint = coordList[2] # based on how created in frontend

    minPointParts = minPoint.split(" ")
    maxPointParts = maxPoint.split(" ")
    return [float(minPointParts[0]), float(minPointParts[1]), float(maxPointParts[0]), float(maxPointParts[1])]

def detect_clear_regions(granuleId):
    granule = Granule(config, granuleId)
    if granule.has_clear_image():
        return
    detector = DecisionTreeDetector(config)
    extractor = Extractor(config, granule)
    
    detector.detect(granule)
    mask = detector.create_clear_mask()
    clearRegions = extractor.extract(mask)
    granule.store_clear_image(clearRegions, mask)


def store_clear_region_images(id, granuleId, region):
    worldFileWriter = Writer(config)
    granule = Granule(config, granuleId)
    if granule.has_request_region_image(id):
        return
    extractor = Extractor(config, granule)
    bbox = get_bbox(region["wkt"])
    
    interestRegion = extractor.extract_geographic_region(bbox, ImageType.CLEAR)
    normalizedBbox = extractor.get_normalized_bbox(bbox) # after extract!
    worldFileWriter.write(10, normalizedBbox, granule, id)
    interestMask = extractor.extract_geographic_region(bbox, ImageType.MASK)
    forNoData = extractor.extract_geographic_region(bbox, ImageType.FOR_NO_DATA)
    granule.store_request_region_image(id, interestRegion)
    granule.store_request_region_mask(id, interestMask)
    granule.store_no_data_mask(id, forNoData)

def get_oldest_coverage_ingestion_time(id):
    sql = '''SELECT to_json(processed_at)::text AS ingestion FROM region_tile
             WHERE region_request_id = %s
             ORDER BY processed_at ASC
             LIMIT 1'''
    cur.execute(sql, [id])
    return cur.fetchone()["ingestion"].replace("\"", "") + 'Z'
         
def get_days_before(timestampString, numDays):
    date = dateutil.parser.parse(timestampString)
    before = date - timedelta(days=numDays)
    beforeIso = before.isoformat()
    beforeParts = beforeIso.split("+")
    return beforeParts[0] + 'Z'

def store_coverage_in_database(id, coverage, region):
    for granuleEntry in coverage:
        print("store granule", granuleEntry.id, id)
        existSql = '''SELECT * FROM region_tile
                      WHERE region_request_id = %s AND granule_id = %s'''
        cur.execute(existSql, [id, granuleEntry.id])
        exists = len(cur.fetchall()) > 0
        if exists:
            continue

        sql = '''INSERT INTO region_tile (granule_id, region_request_id, processed_at, geom, interest_geom)
                 VALUES(%s, %s, %s, ST_GeomFromText(%s, 4326), ST_Intersection(ST_GeomFromText(%s, 4326), ST_GeomFromText(%s, 4326)))'''
        cur.execute(sql, [granuleEntry.id, id, granuleEntry.ingestiondate, granuleEntry.wkt, granuleEntry.wkt, region["wkt"]])
        conn.commit()

def get_coverages_in_database(id):
    sql = '''SELECT * 
            FROM region_tile 
            WHERE region_request_id = %s
            ORDER BY processed_at DESC'''
    cur.execute(sql, [id])
    return cur.fetchall()

def get_granule_entry_set(granuleEntries):
    entryIds = []
    for granuleEntry in granuleEntries:
        entryIds.append(granuleEntry.id)

    return set(entryIds)

def process_region(id, region, manager):
    print(id)

    previousGranulesCovering = []
    granule = Granule(config, "2a56b4de-4241-493c-8e75-57eddc8c859c")
    granule2 = Granule(config, "0616537f-391c-4e3a-a819-023bc0825391")#"0616537f-391c-4e3a-a819-023bc0825391")
    extractor = Extractor(config, granule)
    extractor2 = Extractor(config, granule2)


    storedCoverages = get_coverages_in_database(id)
    for storedCoverage in storedCoverages:
        detect_clear_regions(storedCoverage["granule_id"])
        store_clear_region_images(id, storedCoverage["granule_id"], region)

    #combiner = Combiner(config)
    #combiner.combine_region_images(id, granule, granule2)
    #return
    #combined_masks = combiner.combine_request_interest_region_masks(id, granule, granule2)
    #print("coverage combined:", granule.find_coverage_percentage(id, True, combined_masks))
    #img = combiner.combine(id, granule, granule2)


    #img = extractor.extract_geographic_region([22.4271334006974, 57.545938,22.665185, 57.731191913482], ImageType.ORIGINAL)
    #img2 = extractor2.extract_geographic_region([22.4271334006974, 57.545938,22.665185, 57.731191913482], ImageType.ORIGINAL)
    #img = extractor.extract_geographic_region([21.797250, 57.495085, 21.881207, 57.560848], ImageType.ORIGINAL)
    #print("coverage before:", granule.find_coverage_percentage(id))
    #imageio.imwrite(config.GRANULE_DIRECTORY + "/"  + 'common1.png', img)   
    #imageio.imwrite(config.GRANULE_DIRECTORY + "/"  + 'common2.png', img2)  

    #imageio.imwrite(config.GRANULE_DIRECTORY + "/"  + 'combined.png', img)   

    iterations = 1
    coverage = RequestCoverage(id)
    regionCoords = region["wkt"]
    granulesCovering = find_latest_granule_coverage(coverage, regionCoords, 4, manager)
    while True:
        if manager.should_stop(id) or iterations >= 3:
            pass
            #return;

        print("granules covering region: " + str(len(granulesCovering)))

        download_granule_coverage(id, granulesCovering, manager)

        print("download complete")
        for granuleEntry in granulesCovering:
            detect_clear_regions(granuleEntry.id)

        print("calculated clear regions")
        for granuleEntry in granulesCovering:
            store_clear_region_images(id, granuleEntry.id, region)

        print("store coverage in db")
        store_coverage_in_database(id, granulesCovering, region)


        requestedTileCoverege = RequestTileCoverage(config, id, manager)
        isCoverageComplete = requestedTileCoverege.calculate_tile_coverage()
        if isCoverageComplete:
            requestedTileCoverege.store_finished_coverage()
            return

        print("find next coverages based on empty clear granules")
        oldestTileTime = get_oldest_coverage_ingestion_time(id)
        coverage = RequestCoverage(id)
        iterations = iterations + 1
        timestamp = oldestTileTime
        previousGranuleIds = get_granule_entry_set(granulesCovering)
        while True:
            previousGranulesCovering = granulesCovering
            granulesCovering = find_older_granule_coverage(coverage, region["wkt"], timestamp, manager)
            currentGranuleIds = get_granule_entry_set(granulesCovering)
            timestamp = get_days_before(timestamp, 4)
            if currentGranuleIds != previousGranuleIds:
                break
            else:
                coverage = RequestCoverage(id)
        
        print(oldestTileTime)