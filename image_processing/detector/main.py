from .decision_tree.decision_tree_detector import DecisionTreeDetector
from ..granule.granule import Granule
from ..image_region_extract.extractor import Extractor
import image_processing.app_config.config as config
import imageio

detector = DecisionTreeDetector(config)
granule = Granule(config, "S2B_MSIL1C_20200410T100029_N0209_R122_T34VEJ_20200410T125625.SAFE")

detector.detect(granule)

clear = detector.create_clear_mask()
clouds = detector.create_cloud_mask()

regionExtract = Extractor(config, granule)
cloudy = regionExtract.extract(clouds)
clear = regionExtract.extract(clear)
imageio.imwrite('clouds.png', cloudy)
imageio.imwrite('clear.png', clear)