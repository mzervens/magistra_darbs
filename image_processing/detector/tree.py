class Tree(object):
    def __init__(self, band, value):
        self.left = None
        self.right = None
        self.visited = False
        self.band = band
        self.value = value
        self.maskLeft = None
        self.maskRight = None

    def isLeaf(self):
        return self.left == None and self.right == None


