from ..tree import Tree
from PIL import Image
Image.MAX_IMAGE_PIXELS = 10000000000  
import numpy as np
from skimage import io
import imageio
import cv2


class DecisionTreeDetector:
    def __init__(self, config):
        self.config = config
        self.resampleSize = (5490, 5490)
        self.NO_DATA = 10000
        self.results = []
        self.initTree()
    
    def initTree(self):
        self.tree = Tree("B03", 0.325)
        self.tree.left = Tree("B8A", 0.166)
        self.tree.right = Tree("B11", 0.267)

        self.tree.left.left = Tree("B8A", 0.039)
        self.tree.left.right = Tree("B10", 0.011)

        self.tree.right.left = Tree("B04", 0.647)
        self.tree.right.right = Tree("B07", 1.544)

    def normalize_band(self, band, granule):
        band = cv2.resize(band, self.resampleSize, interpolation=cv2.INTER_CUBIC)
        band[band == granule.NO_DATA] = self.NO_DATA
        return np.true_divide(band, granule.QUANTIFICATION_VALUE)

    def detect(self, granule):
        stack = []
        stack.append(self.tree)
        while stack:
            node = stack.pop()
            if (node.maskLeft == None):
                node.maskLeft = np.full(self.resampleSize, 0, np.uint8)

            if (node.maskRight == None):
                node.maskRight = np.full(self.resampleSize, 0, np.uint8)

            band = granule.get_band_data(node.band)
            band = self.normalize_band(band, granule)

            node.maskLeft[band < node.value] = 1
            band[band == self.NO_DATA] = granule.NO_DATA
            node.maskRight[band > node.value] = 1
            if node.left:
                stack.append(node.left)
            if node.right:
                stack.append(node.right)
            print("processed node " + node.band)

    def create_water_mask(self):
        water = np.multiply(np.multiply(self.tree.maskLeft, self.tree.left.maskLeft), self.tree.left.left.maskLeft)
        water[water > 0] = 1
        return water       

    def create_clear_mask(self):
        clear = np.multiply(np.multiply(self.tree.maskLeft, self.tree.left.maskRight), self.tree.left.right.maskLeft)
        water = self.create_water_mask()
        clear = clear + water
        clear[clear > 0] = 255
        imageio.imwrite('clear_mask.png', clear)
        clear[clear > 0] = 1
        return clear


    def create_cloud_mask(self):
        cirrus = np.multiply(np.multiply(self.tree.maskLeft, self.tree.left.maskRight), self.tree.left.right.maskRight)
        cirrus2 = np.multiply(np.multiply(self.tree.maskRight, self.tree.right.maskLeft), self.tree.right.left.maskLeft)
        clouds = np.multiply(np.multiply(self.tree.maskRight, self.tree.right.maskRight), self.tree.right.right.maskLeft)

        clouds = cirrus + cirrus2 + clouds
        clouds[clouds > 0] = 255
        imageio.imwrite('cloud_mask.png', clouds)
        clouds[clouds > 0] = 1
        return clouds


