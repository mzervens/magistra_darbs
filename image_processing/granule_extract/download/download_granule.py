import os
import requests
import shutil
import zipfile


class DownloadGranule:
    def __init__(self, config):
        self.config = config
        self.baseUrl = "https://scihub.copernicus.eu/dhus/odata/v1/Products('$id')/$value"
        
    def granule_exists(self, id):
        filePath = self.config.GRANULE_DIRECTORY + "/" + id + ".zip"
        return os.path.isfile(filePath)

    def is_granule_extracted(self, id):
        extractPath = self.config.GRANULE_DIRECTORY + "/" + id
        return os.path.isdir(extractPath)

    def extract(self, id):
        extractPath = self.config.GRANULE_DIRECTORY + "/" + id
        if os.path.isdir(extractPath):
            return
        
        os.mkdir(extractPath)
        filePath = extractPath + ".zip"
        with zipfile.ZipFile(filePath, 'r') as zip_ref:
            zip_ref.extractall(extractPath)

        

    def download(self, id):
        if self.granule_exists(id):
            return self.extract(id)

        downloadUrl = self.baseUrl.replace("$id", id)
        filePath = self.config.GRANULE_DIRECTORY + "/" + id + ".zip"
        result = requests.get(downloadUrl, auth=(self.config.SCIHUB_USERNAME, self.config.SCIHUB_PASSWORD), stream=True, timeout=None)
        with open(filePath, 'wb') as out_file:
            shutil.copyfileobj(result.raw, out_file)
        del result
        self.extract(id)


