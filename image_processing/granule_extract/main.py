import image_processing.app_config.config as config
from ..granule_extract.search.search_region import SearchRegion
from ..granule_extract.download.download_granule import DownloadGranule

talsiAoi = '"Intersects(POLYGON((22.548752 57.217378, 22.892962 57.226114, 22.875938 57.440885, 22.268257 57.438113, 22.548752 57.217378)))"'


searchRegion = SearchRegion(config)
searchRegion.find_granules_in_aoi(talsiAoi, 4)

granuleDownloader = DownloadGranule(config)
granuleDownloader.download("dacd5c2a-9bc0-4d72-a4b1-d9195e997753")

