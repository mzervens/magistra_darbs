import requests
from xml.dom import minidom
from enum import Enum

#https://scihub.copernicus.eu/userguide/FullTextSearch

class IngestionTimeType(Enum):
    DAY = 1
    TIMESTAMP = 2

class SearchRegionEntryDataType(Enum):
    XML = 1
    DB = 2


class SearchRegionEntry:
    def __init__(self, entry, dataType = SearchRegionEntryDataType.XML):
        self.entry = entry
        if dataType == SearchRegionEntryDataType.XML:
            self.parse_data()
        else:
            self.parse_db_data()

    def parse_db_data(self):
        self.id = self.entry["granule_id"]
        self.wkt = self.entry["geomwkt"]
        self.ingestiondate = self.entry["processed_at"]

    def parse_data(self):
        attributes = self.entry.getElementsByTagName("str")
        wkt = self.get_element_with_attr_val(attributes, "name", "footprint")
        self.wkt = wkt.childNodes[0].nodeValue

        self.id = self.entry.getElementsByTagName("id")[0].childNodes[0].nodeValue

        dates = self.entry.getElementsByTagName("date")
        ingestiondate = self.get_element_with_attr_val(dates, "name", "ingestiondate")
        self.ingestiondate = ingestiondate.childNodes[0].nodeValue

    def get_element_with_attr_val(self, elements, attribute, value):
        for element in elements:
            if element.getAttribute(attribute) == value:
                return element
        return None


class SearchRegion:
    def __init__(self, config):
        self.config = config
        self.baseUrl = 'https://scihub.copernicus.eu/dhus/search?q='
        self.ingestionFilter = 'ingestiondate:[NOW-$fDAYS TO NOW-$tDAYS]'
        self.orderBy = '&orderby=ingestiondate desc'
        self.productType = 'producttype:S2MSI1C'
        self.platformname = 'platformname:Sentinel-2'
        
    def get_ingestion_filter(self, daysFromNow, daysToNow, timeType = IngestionTimeType.DAY):
        if timeType == IngestionTimeType.DAY:
            return self.ingestionFilter.replace("$f", str(daysFromNow)).replace("$t", str(daysToNow))
        else:
            return self.ingestionFilter.replace("NOW-$fDAYS", str(daysFromNow)).replace("NOW-$tDAYS", str(daysToNow))



    def find_granules_in_aoi(self, aoi, ingestionDaysFrom, ingestionDaysTo = 0, timeType = IngestionTimeType.DAY):
        url = self.baseUrl + 'footprint:' + aoi + self.get_ingestion_filter(ingestionDaysFrom, ingestionDaysTo, timeType)  
        url = url + " AND " + self.productType + " AND " + self.platformname + self.orderBy
        print(url)
        result = requests.get(url, auth=(self.config.SCIHUB_USERNAME, self.config.SCIHUB_PASSWORD))
        xmldoc = minidom.parseString(result.text)
        entries = xmldoc.getElementsByTagName("entry")
        print(len(entries))

        granuleEntries = []
        for entry in entries:
            granuleEntries.append(SearchRegionEntry(entry))
        return granuleEntries

