import cv2
import numpy as np
import math
from pyproj import CRS, Transformer
from enum import Enum
from image_processing.granule.granule import GranuleSizePX, GranuleResolution

class ImageType(Enum):
    CLEAR = 1
    MASK = 2
    ORIGINAL = 3
    INTEREST_CLEAR = 4
    FOR_NO_DATA = 5

class Extractor:
    def __init__(self, config, granule):
        self.config = config
        self.granule = granule
        self.imgDataId = "B13"
        self.imgDataForNoDataId = "B05"
        self.wgs84ToUTM = Transformer.from_crs("EPSG:4326", "EPSG:" + granule.projection)
        self.UTMTowgs84 = Transformer.from_crs("EPSG:" + granule.projection, "EPSG:4326")

    def transform(self, point):
        transf = self.wgs84ToUTM.transform(point[1], point[0])
        return transf

    def transform_bbox_to_wgs84(self, bbox):
        print("transforming from utm", bbox)
        minPoint = self.UTMTowgs84.transform(bbox[0], bbox[1])
        maxPoint = self.UTMTowgs84.transform(bbox[2], bbox[3])
        return [minPoint[0], minPoint[1], maxPoint[0], maxPoint[1]]
        

    def transform_bbox(self, bbox):
        print("transforming to utm", bbox)
        minPoint = self.wgs84ToUTM.transform(bbox[1], bbox[0])
        maxPoint = self.wgs84ToUTM.transform(bbox[3], bbox[2])
        return [minPoint[0], minPoint[1], maxPoint[0], maxPoint[1]]

    def normalize_interest_bbox(self, granuleBBOX, interestBBOX):
        normalized = [0, 0, 0, 0]
        for i, val in enumerate(granuleBBOX):
            if i <= 1:
                if interestBBOX[i] < granuleBBOX[i]:
                    normalized[i] = granuleBBOX[i]
                else: 
                    normalized[i] = interestBBOX[i]
            else:
                if interestBBOX[i] > granuleBBOX[i]:
                    normalized[i] = granuleBBOX[i]
                else: 
                    normalized[i] = interestBBOX[i]
        return normalized

    def get_normalized_bbox(self, interestBbox, transform=True):
        mercatorBBOX = None 
        if transform:
           mercatorBBOX = self.transform_bbox(interestBbox)
        else:
            mercatorBBOX = interestBbox
        return self.normalize_interest_bbox(self.granule.BBOX, mercatorBBOX)

    def get_pixel_coordinates_from_geographic_region(self, bbox, imageShape, granuleBBOX = None, id = None, transform = True, resolution = None):
        if (imageShape[0] == GranuleSizePX.TEN):
            print("here1")
            self.granule.initialize_projection_bbox(GranuleResolution.TEN, GranuleSizePX.TEN)
        elif (imageShape[0] == GranuleSizePX.TWENTY):
            print("here2")
            self.granule.initialize_projection_bbox(GranuleResolution.TWENTY, GranuleSizePX.TWENTY)
        elif (imageShape[0] == GranuleSizePX.SIXTY):
            print("here3")
            self.granule.initialize_projection_bbox(GranuleResolution.SIXTY, GranuleSizePX.SIXTY)

        print("dbg shape", imageShape)

        if transform:
            mercatorBBOX = self.transform_bbox(bbox)
        else: 
            mercatorBBOX = bbox

        mercatorGranuleBBOX = None
        if granuleBBOX != None:
            mercatorGranuleBBOX = self.transform_bbox(granuleBBOX)
        else:
            mercatorGranuleBBOX = self.granule.BBOX

        mercatorBBOX = self.normalize_interest_bbox(mercatorGranuleBBOX, mercatorBBOX)
        
        resolutionX = None 
        resolutionY = None 
        
        if resolution != None:
            resolutionX = resolution
            resolutionY = resolution
        else:
            resolutionX = abs((mercatorGranuleBBOX[0] - mercatorGranuleBBOX[2]) / imageShape[1])
            resolutionY = abs((mercatorGranuleBBOX[1] - mercatorGranuleBBOX[3]) / imageShape[0])

        width = math.floor(abs(mercatorBBOX[0] - mercatorBBOX[2]) / resolutionX)
        height = math.floor(abs(mercatorBBOX[1] - mercatorBBOX[3]) / resolutionY)
        xPixel = math.floor((mercatorBBOX[0] - mercatorGranuleBBOX[0]) / resolutionX)
        yPixel = math.floor((mercatorBBOX[1] - mercatorGranuleBBOX[1]) / resolutionY)
        yPixel = (imageShape[0] - yPixel) - height

        return [yPixel, yPixel+height, xPixel, xPixel+width]

    def extract_geographic_region(self, bbox, imageType, granuleBBOX = None, id = None, transform = True):
        image = None
        if imageType == ImageType.CLEAR:
            image = self.granule.get_clear_image()
        elif imageType == ImageType.MASK:
            image = self.granule.get_clear_mask()
        elif imageType == ImageType.FOR_NO_DATA:
            image = self.granule.get_band_data(self.imgDataForNoDataId)
        elif imageType == ImageType.INTEREST_CLEAR:
            image = self.granule.get_request_region_image(id)
        else:
            image = self.granule.get_band_data(self.imgDataId)

        pixelCoordinates = self.get_pixel_coordinates_from_geographic_region(bbox, image.shape, transform=transform)
        region = image[pixelCoordinates[0]:pixelCoordinates[1], pixelCoordinates[2]:pixelCoordinates[3]]
        return region


    def extract(self, mask):
       image = self.granule.get_band_data(self.imgDataId)
       mask = cv2.resize(mask, (image.shape[1], image.shape[0]), interpolation=cv2.INTER_CUBIC) # cv reverse axis than numpy
       maskRgb = mask[:, :, np.newaxis]
       image = image * maskRgb
       return image

