import io
from image_processing.database.connect import conn, cur
from image_processing.image_region_extract.extractor import Extractor, ImageType
import numpy as np
import cv2
import imageio
from skimage.transform import match_histograms

class Combiner:
    def __init__(self, config):
        self.config = config

    def convert_box_to_bbox(self, boxStr):
        coordStr = boxStr.replace("BOX(", "").replace(")", "")
        parts = coordStr.split(",")
        minCoords = parts[0].split(" ")
        maxCoords = parts[1].split(" ")
        return [float(minCoords[0]), float(minCoords[1]), float(maxCoords[0]), float(maxCoords[1])]

    def get_common_region(self, requestId, granuleId1, granuleId2):
        sql = '''SELECT box2d(ST_Intersection(region_tile.interest_geom, a.interest_geom)) as bbox
                 FROM region_tile,
                 (SELECT interest_geom FROM region_tile WHERE granule_id = %s AND region_request_id = %s)a
                 WHERE granule_id = %s AND region_request_id = %s'''
        #print(requestId, granuleId1, granuleId2)
        cur.execute(sql, [granuleId1, requestId, granuleId2, requestId])
        return self.convert_box_to_bbox(cur.fetchone()["bbox"])

    def get_interest_region_bbox(self, requestId, granuleId):
        sql = '''SELECT box2d(interest_geom) as bbox
                 FROM region_tile 
                 WHERE granule_id = %s AND region_request_id = %s'''
        cur.execute(sql, [granuleId, requestId])
        return self.convert_box_to_bbox(cur.fetchone()["bbox"])      


    def resize(self, image, shape):
        add_x = int((shape[0]-image.shape[0])/2)
        add_y = int((shape[1]-image.shape[1])/2)
        #print(image.shape, shape, add_x, add_y)
        resized = np.zeros(shape, dtype = int)
        resized[:,:,1] = np.pad(image[:,:,1],((add_x,add_x),(add_y,add_y)),'constant', constant_values = (0))
        return resized

    def addRegions(self, image1, image2): # not sure if the logic is correct
        larger = None
        smaller = None
        maxX = max(image1.shape[0], image2.shape[0])
        maxY = max(image1.shape[1], image2.shape[1])
        commonRegion = np.zeros((maxX, maxY), dtype="uint8")

        '''
            if len(image1) > len(image2):
                larger = image1.copy()
                smaller = image2

            elif len(image1) < len(image2):
                larger = image2.copy()
                smaller = image1
            else:
                return image1 + image2
        '''

        commonRegion[:image1.shape[0],:image1.shape[1]] += image1
        commonRegion[:image2.shape[0],:image2.shape[1]] += image2
        return commonRegion

    '''
    def addRegions3(self, image1, image2, clearingMask):
        larger = None
        smaller = None

        clearingMask[clearingMask >= 1] = 2
        clearingMask[clearingMask == 0] = 1
        clearingMask[clearingMask == 2] = 0

        if len(image1) > len(image2):
            larger = image1.copy()
            smaller = image2

        elif len(image1) < len(image2):
            larger = image2.copy()
            smaller = image1
        else:
            clearingMask = cv2.resize(clearingMask, (image2.shape[1], image2.shape[0]), interpolation=cv2.INTER_CUBIC)
            maskRgb = clearingMask[:, :, np.newaxis]
            image2 = image2 * maskRgb
            return image1 + image2

        clearingMask = cv2.resize(clearingMask, (smaller.shape[1], smaller.shape[0]), interpolation=cv2.INTER_CUBIC)
        maskRgb = clearingMask[:, :, np.newaxis]
        smaller = smaller * maskRgb

        larger[:smaller.shape[0],:smaller.shape[1], :] += smaller
        return larger
        '''
    def addRegions3(self, image1, image2, clearingMask): # not sure if the logic is correct
        larger = None
        smaller = None
        maxX = max(image1.shape[0], image2.shape[0])
        maxY = max(image1.shape[1], image2.shape[1])
        commonRegion = np.zeros((maxX, maxY, 3), dtype="uint8")

        clearingMask[clearingMask >= 1] = 2
        clearingMask[clearingMask == 0] = 1
        clearingMask[clearingMask == 2] = 0
        clearingMask = cv2.resize(clearingMask, (image2.shape[1], image2.shape[0]), interpolation=cv2.INTER_CUBIC)
        maskRgb = clearingMask[:, :, np.newaxis]
        image2 = image2 * maskRgb
        '''
            if len(image1) > len(image2):
                larger = image1.copy()
                smaller = image2

            elif len(image1) < len(image2):
                larger = image2.copy()
                smaller = image1
            else:
                return image1 + image2
        '''
        for i in range(3):
            commonRegion[:image1.shape[0],:image1.shape[1],i] += image1[:,:,i]
            commonRegion[:image2.shape[0],:image2.shape[1],i] += image2[:,:,i]
        return commonRegion

    def combine_request_interest_regions(self, requestId, granule1, granule2, useCalcMask = False, calcMask = None):
        commonBbox = self.get_common_region(requestId, granule1.id, granule2.id)
        extractor2 = Extractor(self.config, granule2)
        extractor1 = Extractor(self.config, granule1)


        commonBbox = extractor1.get_normalized_bbox(commonBbox)
        commonBbox = extractor1.transform_bbox_to_wgs84(commonBbox)
        print("normalize 1", commonBbox)
        commonBbox = extractor2.get_normalized_bbox(commonBbox)
        commonBbox = extractor2.transform_bbox_to_wgs84(commonBbox)
        print("normalize 2", commonBbox)

        interestMask1 = None
        if useCalcMask:
            interestMask1 = calcMask
        else:
            interestMask1 = granule1.get_request_region_image(requestId)
        
        clearingMask = granule1.get_request_region_mask(requestId)
        interestMask2 = granule2.get_request_region_image(requestId)

        interestBbox1 = extractor1.transform_bbox(self.get_interest_region_bbox(requestId, granule1.id))
        interestBbox2 = extractor2.transform_bbox(self.get_interest_region_bbox(requestId, granule2.id))

        granule1.set_bbox_value(interestBbox1)
        granule2.set_bbox_value(interestBbox2)
        
        pixelCoords1 = extractor1.get_pixel_coordinates_from_geographic_region(commonBbox, interestMask1.shape, transform=True)
        pixelCoords2 = extractor2.get_pixel_coordinates_from_geographic_region(commonBbox, interestMask2.shape, transform=True)

        clearMask1Region = interestMask1[pixelCoords1[0]:pixelCoords1[1], pixelCoords1[2]:pixelCoords1[3]]
        clearMask2Region = interestMask2[pixelCoords2[0]:pixelCoords2[1], pixelCoords2[2]:pixelCoords2[3]]
        clearMask1Shape = clearMask1Region.shape

        clearMask1Added = self.addRegions3(clearMask1Region, clearMask2Region, clearingMask) # clearMask1Region + clearMask2Region
        #clearMask1Added[clearMask1Added > 1] = 1
        clearMask1Added.resize(clearMask1Shape)

        interestMask1[pixelCoords1[0]:pixelCoords1[1], pixelCoords1[2]:pixelCoords1[3]] = clearMask1Added
        
        return interestMask1

    def combine_request_interest_region_masks(self, requestId, granule1, granule2, useCalcMask = False, calcMask = None):
        commonBbox = self.get_common_region(requestId, granule1.id, granule2.id)
        extractor2 = Extractor(self.config, granule2)
        extractor1 = Extractor(self.config, granule1)

        interestMask1 = None
        if useCalcMask:
            interestMask1 = calcMask
        else:
            interestMask1 = granule1.get_request_region_mask(requestId)
        
        interestMask2 = granule2.get_request_region_mask(requestId)

        interestBbox1 = extractor1.transform_bbox(self.get_interest_region_bbox(requestId, granule1.id))
        interestBbox2 = extractor2.transform_bbox(self.get_interest_region_bbox(requestId, granule2.id))

        granule1.set_bbox_value(interestBbox1)
        granule2.set_bbox_value(interestBbox2)
        
        pixelCoords1 = extractor1.get_pixel_coordinates_from_geographic_region(commonBbox, interestMask1.shape)
        pixelCoords2 = extractor2.get_pixel_coordinates_from_geographic_region(commonBbox, interestMask2.shape)

        clearMask1Region = interestMask1[pixelCoords1[0]:pixelCoords1[1], pixelCoords1[2]:pixelCoords1[3]]
        clearMask2Region = interestMask2[pixelCoords2[0]:pixelCoords2[1], pixelCoords2[2]:pixelCoords2[3]]
        clearMask1Shape = clearMask1Region.shape

        clearMask1Added = self.addRegions(clearMask1Region, clearMask2Region) # clearMask1Region + clearMask2Region
        clearMask1Added[clearMask1Added > 1] = 1
        clearMask1Added.resize(clearMask1Shape)

        interestMask1[pixelCoords1[0]:pixelCoords1[1], pixelCoords1[2]:pixelCoords1[3]] += clearMask1Added
        interestMask1[interestMask1 > 1] = 1
        
        return interestMask1


    def combine_masks(self, requestId, granule1, granule2):
        commonBbox = self.get_common_region(requestId, granule1.id, granule2.id)
        extractor2 = Extractor(self.config, granule2)
        extractor1 = Extractor(self.config, granule1)

        commonImg = extractor2.extract_geographic_region(commonBbox, ImageType.MASK)
        pxCoords1 = extractor1.get_pixel_coordinates_from_geographic_region(commonBbox, (5490, 5490))
        clearMask1 = granule1.get_clear_mask()
        clearMask1Region = clearMask1[pxCoords1[0]:pxCoords1[1], pxCoords1[2]:pxCoords1[3]]
        clearMask1Region = clearMask1Region + commonImg
        clearMask1Region[clearMask1Region > 1] = 1
        clearMask1[pxCoords1[0]:pxCoords1[1], pxCoords1[2]:pxCoords1[3]] = clearMask1Region
        
        return clearMask1Region

    def swapMaskValues(self, mask, shape):
        mask[mask >= 1] = 2
        mask[mask == 0] = 1
        mask[mask == 2] = 0
        mask = cv2.resize(mask, (shape[1], shape[0]), interpolation=cv2.INTER_CUBIC)
        #maskRgb = np.broadcast_to(mask == 1, shape)
        maskRgb = mask[:, :, np.newaxis]
        return maskRgb

    def combine_region_images(self, requestId, granule1, granule2, useCombinedImage = False, combinedImg = None):
        commonBbox = self.get_common_region(requestId, granule1.id, granule2.id)
        extractor2 = Extractor(self.config, granule2)
        extractor1 = Extractor(self.config, granule1)

        interestBbox1 = granule1.get_request_region_bbox(requestId)#extractor1.transform_bbox(self.get_interest_region_bbox(requestId, granule1.id))
        interestBbox2 = granule2.get_request_region_bbox(requestId)#extractor2.transform_bbox(self.get_interest_region_bbox(requestId, granule2.id))
        print(interestBbox1, interestBbox2)

        granule1.set_bbox_value(interestBbox1)
        granule2.set_bbox_value(interestBbox2)

        commonBbox = extractor1.get_normalized_bbox(commonBbox)
        commonBbox = extractor1.transform_bbox_to_wgs84(commonBbox)
        print("normalize 1", commonBbox)
        commonBbox = extractor2.get_normalized_bbox(commonBbox)
        commonBbox = extractor2.transform_bbox_to_wgs84(commonBbox)
        print("normalize 2", commonBbox)




        regionImg1 = None
        if useCombinedImage:
            regionImg1 = combinedImg
        else:
            regionImg1 = granule1.get_request_region_image(requestId)
        
        regionImg2 = granule2.get_request_region_image(requestId)

        pixelCoordinatesIn1 = extractor1.get_pixel_coordinates_from_geographic_region(commonBbox, regionImg1.shape, transform=True, resolution=10.0)
        pixelCoordinatesIn2 = extractor2.get_pixel_coordinates_from_geographic_region(commonBbox, regionImg2.shape, transform=True, resolution=10.0)

        mask1 = None
        mask2 = granule2.get_request_region_mask_scaled(requestId, regionImg2.shape)

        if useCombinedImage:
            mask1 = granule1.get_request_region_combining_mask(requestId)
        else:
            mask1 = granule1.get_request_region_mask_scaled(requestId, regionImg1.shape)

        commonMask1 = mask1[pixelCoordinatesIn1[0]:pixelCoordinatesIn1[1], pixelCoordinatesIn1[2]:pixelCoordinatesIn1[3]]
        commonMask2 = mask2[pixelCoordinatesIn2[0]:pixelCoordinatesIn2[1], pixelCoordinatesIn2[2]:pixelCoordinatesIn2[3]]
        commonMask2Copy = np.copy(commonMask2)
        commonMask2[commonMask1 >= 1] = 0 

        commonMask2Rgb = commonMask2[:, :, np.newaxis]

        commonRegImg1 = regionImg1[pixelCoordinatesIn1[0]:pixelCoordinatesIn1[1], pixelCoordinatesIn1[2]:pixelCoordinatesIn1[3]]
        commonRegImg2 = regionImg2[pixelCoordinatesIn2[0]:pixelCoordinatesIn2[1], pixelCoordinatesIn2[2]:pixelCoordinatesIn2[3]] * commonMask2Rgb 
        
        print("combining ", granule1.id, granule2.id, commonMask1.shape)
        print("common mask ", commonMask2.shape, mask1.shape, mask2.shape)
        mask1[pixelCoordinatesIn1[0]:pixelCoordinatesIn1[1], pixelCoordinatesIn1[2]:pixelCoordinatesIn1[3]] = commonMask2Copy
        mask1[mask1 > 1] = 1
        granule1.store_request_region_combining_mask(requestId, mask1)

        combinedRegion = commonRegImg1 + commonRegImg2
        combinedRegion = self.match_histogram(regionImg1, combinedRegion)
        regionImg1[pixelCoordinatesIn1[0]:pixelCoordinatesIn1[1], pixelCoordinatesIn1[2]:pixelCoordinatesIn1[3]] = combinedRegion
        #imageio.imwrite(self.config.GRANULE_DIRECTORY + "/"  + 'combined.png', regionImg1) 
        return regionImg1
        #print("pix coords 1 ", pixelCoordinatesIn1)
        #print("pix coords 2 ", pixelCoordinatesIn2)
        #imageio.imwrite(self.config.GRANULE_DIRECTORY + "/"  + 'c1.png', commonRegImg1)  
        #imageio.imwrite(self.config.GRANULE_DIRECTORY + "/"  + 'c2.png', commonRegImg2)  
        #imageio.imwrite(self.config.GRANULE_DIRECTORY + "/"  + 'combined.png', regionImg1) 

    def match_histogram(self, reference, image):
        matched = match_histograms(image, reference, multichannel=True)
        return matched

    def combine(self, requestId, granule1, granule2, useCombinedImg = False, combinedImg = None, combinedMask = None):
        commonBbox = self.get_common_region(requestId, granule1.id, granule2.id)
        extractor2 = Extractor(self.config, granule2)
        extractor1 = Extractor(self.config, granule1)


        commonBbox = extractor1.get_normalized_bbox(commonBbox)
        commonBbox = extractor1.transform_bbox_to_wgs84(commonBbox)
        print("normalize 1", commonBbox)
        commonBbox = extractor2.get_normalized_bbox(commonBbox)
        commonBbox = extractor2.transform_bbox_to_wgs84(commonBbox)
        print("normalize 2", commonBbox)



        commonImg = extractor2.extract_geographic_region(commonBbox, ImageType.CLEAR, transform=True)
        commonMask = extractor2.extract_geographic_region(commonBbox, ImageType.MASK, transform=True)
        print("common img shape", commonImg.shape)
        commonMask = self.swapMaskValues(commonMask, commonImg.shape)
        
        commonImg = commonImg * commonMask
        print("common img shape after", commonImg.shape)
        pixelCoordinatesIn1 = extractor1.get_pixel_coordinates_from_geographic_region(commonBbox, (10980, 10980), transform=True)
        commonImgDbg = extractor1.extract_geographic_region(commonBbox, ImageType.CLEAR, transform=True)
        print("dbg shape", commonImgDbg.shape)
        clearImg1 = None 
        
        if useCombinedImg:
            clearImg1 = combinedImg
        else:
            clearImg1 = granule1.get_clear_image()
        clearImg1[pixelCoordinatesIn1[0]:pixelCoordinatesIn1[1], pixelCoordinatesIn1[2]:pixelCoordinatesIn1[3]] += commonImg
        return (clearImg1, commonMask)

