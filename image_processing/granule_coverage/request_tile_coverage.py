from image_processing.database.connect import conn, cur
from image_processing.granule.granule import Granule
from image_processing.image_region_extract.extractor import Extractor, ImageType
from image_processing.world_file.writer import Writer
from image_processing.granule_extract.search.search_region import SearchRegion, SearchRegionEntry, SearchRegionEntryDataType, IngestionTimeType
from image_processing.granule_coverage.request_coverage import RequestCoverage
from image_processing.image_region_extract.combiner import Combiner
import imageio

class RequestTileCoverage:
    def __init__(self, config, requestId, manager, minimumCoveragePercent = 0.93):
        self.requestId = requestId
        self.config = config
        self.manager = manager
        self.minimumCoverage = minimumCoveragePercent

    def find_latest_granule_coverage_in_db(self, regionTiles):
        coverage = RequestCoverage(self.requestId)
        regionEntries = []
        for regionTile in regionTiles:
            granuleEntry = [SearchRegionEntry(regionTile, SearchRegionEntryDataType.DB)]
            coverage.add_granules_covering_region(granuleEntry, self.manager)
            regionEntries.append(regionTile)
            if coverage.is_coverage_complete():
                return regionEntries
        return None

    def get_intersecting_tiles(self, tileId):
        sql = '''SELECT id, granule_id FROM region_tile,
                (SELECT interest_geom FROM region_tile
                 WHERE id = %s)a
                 WHERE id != %s AND ST_Intersects(a.interest_geom, region_tile.interest_geom)
                 ORDER BY processed_at DESC,
                 ST_Area(ST_Intersection(a.interest_geom, region_tile.interest_geom)) DESC'''
        cur.execute(sql, [tileId, tileId])
        return cur.fetchall()

    def get_tiles(self):
        sql = '''SELECT *, ST_AsText(geom) AS geomwkt FROM region_tile
                 WHERE region_request_id = %s
                 ORDER BY processed_at DESC,
                 ST_Area(interest_geom) DESC'''
        cur.execute(sql, [self.requestId])
        return cur.fetchall()

    def get_common_region(self, tileId1, tileId2):
        sql = '''SELECT Box2D(ST_Intersection(region_tile.interest_geom, a.interest_geom)) as bbox FROM region_tile,
                (SELECT interest_geom FROM region_tile 
                 WHERE id = %s)a
                 WHERE id = %s'''
        cur.execute(sql, [tileId1, tileId2])
        return cur.fetchone()["bbox"]

    def convert_box_to_bbox(self, boxStr):
        coordStr = boxStr.replace("BOX(", "").replace(")", "")
        parts = coordStr.split(",")
        minCoords = parts[0].split(" ")
        maxCoords = parts[1].split(" ")
        return [float(minCoords[0]), float(minCoords[1]), float(maxCoords[0]), float(maxCoords[1])]

    def intersects_tile_with_good_coverage(self, coveragePercentages, tileId):
        intersecting = self.get_intersecting_tiles(tileId)
        for tile in intersecting:
            if not tile["id"] in coveragePercentages:
                continue

            perc = coveragePercentages[tile["id"]]
            if perc >= self.minimumCoverage:
                return True
        return False

    def calculate_tile_coverage(self):
        combiner = Combiner(self.config)
        tileCoverages = {}
        tiles = self.get_tiles()
        latestCoverageTiles = self.find_latest_granule_coverage_in_db(tiles)
        coverageEnough = True
        if latestCoverageTiles == None:
            return False

        for tile in latestCoverageTiles:
            granule = Granule(self.config, tile["granule_id"])
            intersectingTiles = self.get_intersecting_tiles(tile["id"])
            combinedMask = None
            i = 0
            for intersectingTile in intersectingTiles:
                granule2 = Granule(self.config, intersectingTile["granule_id"])
                if granule.projection != granule2.projection: # this would require the reprojection of the image which will make the processing much more complex
                    continue
                if i == 0:
                    combinedMask = combiner.combine_request_interest_region_masks(self.requestId, granule, granule2)
                else:
                    combinedMask = combiner.combine_request_interest_region_masks(self.requestId, granule, granule2, True, combinedMask)
                i = i + 1
            coverage = 0
            if i > 0:
                coverage = granule.find_coverage_percentage(self.requestId, True, combinedMask)
            else:
                coverage = granule.find_coverage_percentage(self.requestId)
            tileCoverages[tile["id"]] = coverage
            isEnough = coverage >= self.minimumCoverage or self.intersects_tile_with_good_coverage(tileCoverages, tile["id"])
            coverageEnough = isEnough and coverageEnough
            print("coverage combined for :", tile["id"], coverage, isEnough)
        
        return coverageEnough

    def store_finished_coverage(self):
        combiner = Combiner(self.config)
        tileCoverages = {}
        tiles = self.get_tiles()
        latestCoverageTiles = self.find_latest_granule_coverage_in_db(tiles)
        coverageEnough = True
        if latestCoverageTiles == None:
            return False

        for tile in latestCoverageTiles:
            granule = Granule(self.config, tile["granule_id"])
            intersectingTiles = self.get_intersecting_tiles(tile["id"])
            combinedMask = None
            i = 0
            for intersectingTile in intersectingTiles:
                granule2 = Granule(self.config, intersectingTile["granule_id"])
                if granule.projection != granule2.projection: # this would require the reprojection of the image which will make the processing much more complex
                    continue
                if i == 0:
                    #combinedMask = combiner.combine(self.requestId, granule, granule2, useCombinedImg = False, combinedImg = None)
                    #combinedMask = combiner.combine_request_interest_regions(self.requestId, granule, granule2)
                    combinedMask = combiner.combine_region_images(self.requestId, granule, granule2, useCombinedImage = False, combinedImg = None)
                else:
                    #combinedMask = combiner.combine(self.requestId, granule, granule2, True, combinedMask)
                    #combinedMask = combiner.combine_request_interest_regions(self.requestId, granule, granule2, True, combinedMask)
                    combinedMask = combiner.combine_region_images(self.requestId, granule, granule2, True, combinedMask)
                i = i + 1


            granule.store_finished_request_image(self.requestId, combinedMask)
