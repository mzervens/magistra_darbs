from image_processing.database.connect import conn, cur


class RequestCoverage:
    def __init__(self, requestId):
        self.id = requestId
        self.granuleCoverage = []
        self.clear_coverage()

    def get_granules_covering_region(self):
        return self.granuleCoverage

    def clear_coverage(self):
        sql = "UPDATE region_request SET granule_coverage_difference = geom WHERE id = %s"
        cur.execute(sql, [self.id])
        conn.commit()

    def is_coverage_complete(self):
        sql = '''SELECT ST_IsEmpty(granule_coverage_difference) AS covered
             FROM region_request
             WHERE id = %s'''
        cur.execute(sql, [self.id])
        return cur.fetchone()["covered"]

    def get_granule_coverage_difference(self):
        sql = '''SELECT ST_AsText(granule_coverage_difference) as wkt
                 FROM region_request WHERE id = %s'''
        cur.execute(sql, [self.id])
        return cur.fetchone()["wkt"]

    def check_if_granule_intersects(self, entry):
        sql = '''SELECT NOT ST_IsEmpty(granule_coverage_difference) AND NOT ST_Equals( granule_coverage_difference, 
                                    ST_Difference(granule_coverage_difference, 
                                                ST_GeomFromText(%s, 4326))) AS intersects 
                FROM region_request WHERE id = %s'''
        cur.execute(sql, [entry.wkt, self.id])
        intersects = cur.fetchone()["intersects"]
        return intersects

    def update_granule_coverage(self, entry):
        sql = '''UPDATE region_request 
                SET granule_coverage_difference = ST_Difference(granule_coverage_difference, 
                                                                ST_GeomFromText(%s, 4326))
                WHERE id = %s'''
        cur.execute(sql, [entry.wkt, self.id])
        conn.commit()

    def add_granules_covering_region(self, granuleEntries, manager):
        for entry in granuleEntries:
            if self.check_if_granule_intersects(entry):
                self.granuleCoverage.append(entry)
                self.update_granule_coverage(entry)