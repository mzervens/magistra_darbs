import os

class Reader:
    def __init__(self, config):
        self.config = config

    def read(self, granule, requestId):
        path = os.path.dirname(granule.get_band_path("B01"))
        with open(path + "/"+str(requestId) + ".pgw", 'r') as file:
            data = file.read().split('\n')
            return {"resolution": float(data[0]), "xMin": float(data[4]), "yMax": float(data[5])}
