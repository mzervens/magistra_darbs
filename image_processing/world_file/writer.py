import os


class Writer:
    def __init__(self, config):
        self.config = config

    def write(self, resolution, bbox, granule, requestId):
        xCoord = bbox[0];
        yCoord = bbox[3];

        content = str(resolution) + "\n" + "0.0" + "\n" + "0.0" + "\n" + "-" + str(resolution) + "\n" + str(xCoord) + "\n" + str(yCoord) + "\n";
        path = os.path.dirname(granule.get_band_path("B01"))
        f = open(path + "/"+str(requestId)+".pgw", "a")
        f.write(content)
        f.close()

